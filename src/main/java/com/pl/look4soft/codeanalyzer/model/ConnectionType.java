package com.pl.look4soft.codeanalyzer.model;

/**
 * Created by lukaszwrona on 23.09.15.
 */
public class ConnectionType {
    private String connectionType;

    public ConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }
}
