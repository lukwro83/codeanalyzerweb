package com.pl.look4soft.codeanalyzer.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.pl.look4soft.codeanalyzer.enums.ConnectionTypesEnum;

import java.io.Serializable;

/**
 * Created by lukaszwrona on 23.09.15.
 */
@Entity
public class ConnectionData implements Serializable, Cloneable{

    @Id
    private Long id;
    private String connectionName;
    private String url;
    private String userName;
    private String password;
    private ConnectionTypesEnum connectionType;

    public ConnectionData() {
    }

    public ConnectionData(Long id, String connectionName, String url, String userName, String password, ConnectionTypesEnum connectionType) {
        this.id = id;
        this.connectionName = connectionName;
        this.url = url;
        this.userName = userName;
        this.password = password;
        this.connectionType = connectionType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ConnectionTypesEnum getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ConnectionTypesEnum connectionType) {
        this.connectionType = connectionType;
    }
}
