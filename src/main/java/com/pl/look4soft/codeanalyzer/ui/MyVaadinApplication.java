package com.pl.look4soft.codeanalyzer.ui;

import com.googlecode.objectify.ObjectifyService;
import com.pl.look4soft.codeanalyzer.enums.ConnectionTypesEnum;
import com.pl.look4soft.codeanalyzer.model.ConnectionData;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.SelectionEvent;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.GAEVaadinServlet;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer;

import javax.servlet.annotation.WebServlet;
import java.util.List;

/**
 * Created by lukaszwrona on 23.09.15.
 */
@Title("Code Analyzer")
@Theme("valo")
public class MyVaadinApplication extends UI implements View {

    Button newContact = new Button("Add/Edit connections");
    TextField filter = new TextField();
    ConnectionForm connectionForm = new ConnectionForm();
    Grid connectionList = new Grid();

    @Override
    public void init(VaadinRequest request) {
        initComponents();
        buildLayout();
    }

    private void initComponents(){
        // Create a navigator to control the views
        newContact.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                connectionForm.setVisible(true);
                connectionForm.edit(new ConnectionData());
            }
        });
        List<ConnectionData> list = ObjectifyService.ofy().load().type(ConnectionData.class).list();
        BeanItemContainer<ConnectionData> conData = new BeanItemContainer<>(ConnectionData.class, list);
        GeneratedPropertyContainer gpc =
                new GeneratedPropertyContainer(conData);
        gpc.addGeneratedProperty("Check Metrics",
                new PropertyValueGenerator<String>() {

                    @Override
                    public String getValue(Item item, Object itemId,
                                           Object propertyId) {
                        return "Code Metrics";
                    }

                    @Override
                    public Class<String> getType() {
                        return String.class;
                    }
                });
        connectionList.setContainerDataSource(gpc);
        connectionList.getColumn("Check Metrics").setRenderer(new ButtonRenderer(new ClickableRenderer.RendererClickListener() {
            @Override
            public void click(ClickableRenderer.RendererClickEvent rendererClickEvent) {
                MyVaadinApplication.getCurrent().getPage().setLocation(getUI().getPage().getLocation() + "CodeSchema" + "?" + "conId=" + ((ConnectionData)connectionList.getSelectedRow()).getId());

            }
        }));
        connectionList.setColumnOrder("connectionName");
        //connectionList.removeColumn("id");
        connectionList.removeColumn("userName");
        connectionList.removeColumn("password");
        connectionList.setSelectionMode(Grid.SelectionMode.SINGLE);
        connectionList.addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
             public void select(SelectionEvent selectionEvent) {
                 connectionForm.edit((ConnectionData) connectionList.getSelectedRow());
             }
         });
    }

    private void buildLayout(){
        HorizontalLayout actions = new HorizontalLayout(filter, newContact);
        actions.setWidth("100%");
        filter.setWidth("100%");
        actions.setExpandRatio(filter, 1);

        VerticalLayout left = new VerticalLayout(actions, connectionList);
        left.setSizeFull();
        connectionList.setSizeFull();
        left.setExpandRatio(connectionList, 1);

        HorizontalLayout mainLayout = new HorizontalLayout(left, connectionForm);
        mainLayout.setSizeFull();
        mainLayout.setExpandRatio(left, 1);
        connectionForm.setVisible(false);
        // Split and allow resizing
        setContent(mainLayout);
    }

    public void refreshList(){
        refreshContacts(filter.getValue());
    }

    private void refreshContacts(String value) {
        connectionList.setContainerDataSource(new BeanItemContainer<>(ConnectionData.class, ObjectifyService.ofy().load().type(ConnectionData.class).list()));
        connectionForm.setVisible(false);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    @WebServlet(urlPatterns = "/*", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyVaadinApplication.class, productionMode = false)
    public static class MyUIServlet extends GAEVaadinServlet {
    }

}
