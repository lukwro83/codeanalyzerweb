package com.pl.look4soft.codeanalyzer.ui;

import com.pl.look4soft.codeanalyzer.enums.ConnectionTypesEnum;
import com.pl.look4soft.codeanalyzer.model.ConnectionData;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

/**
 * Created by lukaszwrona on 23.09.15.
 */
public class ConnectionDataUI extends FormLayout {


    TextField connectionName = new TextField("Connection Name");
    TextField url = new TextField("Url");
    TextField userName = new TextField("name");
    TextField password = new TextField("password");
    ComboBox connectionType = new ComboBox("Connection Type");



    public ConnectionDataUI() {
        buildLayout();
        initComponents();
    }

    private void initComponents(){
        connectionName.setRequired(true);
        url.setRequired(true);
        connectionName.setNullRepresentation("");
        url.setNullRepresentation("");
        userName.setNullRepresentation("");
        password.setNullRepresentation("");
        connectionName.setNullSettingAllowed(false);
        url.setNullSettingAllowed(false);
        connectionType.setNullSelectionAllowed(false);
        for(ConnectionTypesEnum type : ConnectionTypesEnum.values()){
            connectionType.addItem(type);
        }
        connectionType.setRequired(true);
    }

    private void buildLayout() {
        setSizeUndefined();
        setMargin(true);


        addComponents(connectionType, connectionName, url, userName, password);
    }
}
