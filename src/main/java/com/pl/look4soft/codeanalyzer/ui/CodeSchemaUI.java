package com.pl.look4soft.codeanalyzer.ui;

import com.googlecode.objectify.ObjectifyService;
import com.pl.look4soft.codeanalyzer.model.ConnectionData;
import com.pl.look4soft.codeanalyzer.services.bitBucket.JSONServiceBitBucket;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.GAEVaadinServlet;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;

/**
 * Created by lukaszwrona on 27.09.15.
 */
@Title("Code Analyzer")
@Theme("valo")
public class CodeSchemaUI extends UI implements View {

    Button newContact = new Button("Back to connection List");
    TextField id = new TextField("ID");
    TextArea value = new TextArea("value");
    TextArea src = new TextArea("src");

    //@Inject
    //JSONServiceBitBucket jsonServiceBitBucket;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        initComponents(vaadinRequest);
        buildLayout();
    }

    private void initComponents(VaadinRequest vaadinRequest){
        ConnectionData data = ObjectifyService.ofy().load().type(ConnectionData.class).id(Long.valueOf(vaadinRequest.getParameter("conId"))).now();
        id.setValue(data.getConnectionName());
        newContact.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

            }
        });
        JSONServiceBitBucket jsonServiceBitBucket = new JSONServiceBitBucket();
         src.setValue(jsonServiceBitBucket.getCommits("dd"));
         value.setValue(jsonServiceBitBucket.getSchema("dd"));
    }

    private void buildLayout(){
        HorizontalLayout mainLayout = new HorizontalLayout(newContact, id, value, src);
        setContent(mainLayout);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    @WebServlet(urlPatterns = "/CodeSchema", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyVaadinApplication.class, productionMode = false)
    public static class MyUIServlet extends GAEVaadinServlet {
    }
}
