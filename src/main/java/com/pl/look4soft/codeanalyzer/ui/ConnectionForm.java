package com.pl.look4soft.codeanalyzer.ui;

import com.googlecode.objectify.ObjectifyService;
import com.pl.look4soft.codeanalyzer.enums.ConnectionTypesEnum;
import com.pl.look4soft.codeanalyzer.model.ConnectionData;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.ui.*;

/**
 * Created by lukaszwrona on 23.09.15.
 */
public class ConnectionForm extends FormLayout{
    Button save = new Button("Save");
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete");

    ConnectionDataUI connectionDatas = new ConnectionDataUI();
    BeanFieldGroup<ConnectionData> formFieldBindings;
    ConnectionData connectionData;

    public ConnectionForm() {
        buildLayout();
        initComponents();
    }

    private void initComponents(){

        save.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                save(clickEvent);
            }
        });
        cancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                cancel(clickEvent);
            }
        });
        delete.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ObjectifyService.ofy().delete().entity(connectionData).now();
                getUI().refreshList();
            }
        });
    }


    private void buildLayout(){
        setSizeUndefined();
        setMargin(true);

        HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);
        actions.setSpacing(true);

        HorizontalLayout connectionData = new HorizontalLayout(connectionDatas);
        VerticalLayout down = new VerticalLayout(actions, connectionData);

        addComponents(down);
    }

    public void save(Button.ClickEvent event) {
        try {
            formFieldBindings.commit();
            ObjectifyService.ofy().save().entity(connectionData).now();
            getUI().refreshList();
        } catch (FieldGroup.CommitException e) {
            e.printStackTrace();
        }

    }

    public void edit(ConnectionData data){
        this.connectionData= data;
        if (data != null){
            formFieldBindings = BeanFieldGroup.bindFieldsBuffered(data, connectionDatas);
        }
        setVisible(data != null);
    }

    public void cancel(Button.ClickEvent event) {
        this.setVisible(false);
    }

    @Override
    public MyVaadinApplication getUI() {
        return (MyVaadinApplication) super.getUI();
    }
}
