package com.pl.look4soft.codeanalyzer.services.bitBucket;

import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;


/**
 * Created by lukaszwrona on 27.09.15.
 */
@ApplicationScoped
public class JSONServiceBitBucket {

    RestTemplate restTemplate = new RestTemplate();


    public  String getCommits(String path){
        String valueJSON = restTemplate.getForObject("https://api.bitbucket.org/2.0/repositories/lukwro83/tasksheduling/commits", String.class);
        return valueJSON;
    }

    public  String getSchema(String path){
        String valueJSON = restTemplate.getForObject("https://bitbucket.org/api/1.0/repositories/lukwro83/tasksheduling/src/9c4badc501c6bbfd5f6000e74c1963092b71033f/", String.class);
        return valueJSON;
    }

}
